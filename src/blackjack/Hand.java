package blackjack;

import java.util.ArrayList;

public class Hand {

    private ArrayList<Card> carHandCards;
    private boolean bDealer;


    public Hand(boolean bDealer) {
        this.bDealer = bDealer;
        carHandCards = new ArrayList<Card>();

    }


    public void hit(Card carHit) {
        carHandCards.add(carHit);
    }


    public int getHardValue() {
        //add up the values of hte cards on face value; aces are 11
        int nRet = 0;
        for (Card car : carHandCards) {

            nRet += car.getValue();
        }
        return nRet;

    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();
        int nC = 0;
        for (Card carHandCard : carHandCards) {
            if (nC++ == 0 && bDealer)
                stringBuilder.append("##" + " ");
            else
                stringBuilder.append(carHandCard.toString() + " ");
        }

        if (!bDealer)
            stringBuilder.append(getHardValue());

        return stringBuilder.toString();

    }


    public String showEndState() {

        StringBuilder stringBuilder = new StringBuilder();
        for (Card carHandCard : carHandCards) {
            stringBuilder.append(carHandCard.toString() + " ");
        }
        stringBuilder.append(getHardValue());
        return stringBuilder.toString();

    }
}
