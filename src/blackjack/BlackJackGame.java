package blackjack;
// Add cards image later.

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BlackJackGame {
    private JPanel jPanel;
    private JButton dealButton;
    private JButton standButton;
    private JButton hitButton;
    private JButton button100;
    private JButton button50;
    private JTextField playValue;
    private JTextField dealerValue;
    private JTextField playerMoney;
    private JButton startButton;
    private JTextField overResult;
    private JLabel Instruction;

    private BlackJack blackJack;

    public static void main(String[] args) {
        JFrame frame = new JFrame("BlackJackGame");
        frame.setContentPane(new BlackJackGame().jPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(600, 400);
        frame.setVisible(true);
    }

    public BlackJackGame() {
        blackJack = new BlackJack();
        button50.setEnabled(false);
        startButton.setEnabled(true);
        hitButton.setEnabled(false);
        dealButton.setEnabled(false);
        standButton.setEnabled(false);
        button100.setEnabled(false);
        startButton.addActionListener(new StartActionListener());
        button50.addActionListener(new BetActionListener50());
        button100.addActionListener(new BetActionListener100());
        dealButton.addActionListener(new DealActionListener());
        standButton.addActionListener(new StandActionListener());
        hitButton.addActionListener(new HitActionListener());
    }


    class StartActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            dealButton.setEnabled(true);
            startButton.setEnabled(false);
            button50.setEnabled(true);
            button100.setEnabled(true);
            playValue.setText("");
            dealerValue.setText("");
            overResult.setText("");
            playerMoney.setText("");
            blackJack.getPly().resetMoney();
        }
    }

    class DealActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            button50.setEnabled(false);
            button100.setEnabled(false);
            playValue.setText(blackJack.statusHanPlayer());
            dealerValue.setText(blackJack.statusHanDealer());
            hitButton.setEnabled(true);
            standButton.setEnabled(true);
            dealButton.setEnabled(false);
        }

    }

    class HitActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            blackJack.getDealer().hitPlayer();
            playValue.setText(blackJack.statusHanPlayer());
            if (blackJack.getHanPlayer().getHardValue() > 21){
                overResult.setText(blackJack.showStatusAfterHit());
                startButton.setEnabled(true);
                blackJack.initHands();
                hitButton.setEnabled(false);
                standButton.setEnabled(false);
            }

        }
    }
    class StandActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            blackJack.getDealer().autoHit();
            hitButton.setEnabled(false);
            dealButton.setEnabled(false);
            standButton.setEnabled(false);
            startButton.setEnabled(true);
            overResult.setText(blackJack.showStatusAfterDealerAutoHit());
            dealerValue.setText(blackJack.statusHanDealer());
        }

    }

    class BetActionListener50 implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            blackJack.setMoney(50.00);
            playerMoney.setText(blackJack.getPly().getStringMoney());
        }
    }

    class BetActionListener100 implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            blackJack.getPly().setMoney(100.00);
            playerMoney.setText(blackJack.getPly().getStringMoney());
        }
    }
}