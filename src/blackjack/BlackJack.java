package blackjack;

public class BlackJack {

    private Dealer dlr;
    private Player ply;
    private Hand hanDealer;
    private Hand hanPlayer;
    private Shoe sho;


    public BlackJack() {
        initialize();
    }

    private void initialize() {

        ply = new Player(0.00);
        sho = new Shoe();
        initHands();

    }

    public void initHands() {
        hanPlayer = new Hand(false);
        hanDealer = new Hand(true);
        dlr = new Dealer(sho, hanDealer, hanPlayer);
        dlr.hitDealer();
        dlr.hitDealer();
        dlr.hitPlayer();
        dlr.hitPlayer();
    }


    public String statusHanDealer() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Dealer has:" + hanDealer.toString());
        return stringBuilder.toString();

    }
    public String statusHanPlayer() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("You have  :" + hanPlayer.toString());
        return stringBuilder.toString();
    }

    public Dealer getDealer() {
        return dlr;
    }

    public String showStatusAfterHit() {

        StringBuilder stringBuilder = new StringBuilder();

        if (hanPlayer.getHardValue() > 21) {
            stringBuilder.append("You BUSTED, YOU LOSE: ");
            return showResult(stringBuilder);
        }
        return "";

    }

    public String showStatusAfterDealerAutoHit() {

        StringBuilder stringBuilder = new StringBuilder();
        if (hanDealer.getHardValue() > 21) {
            stringBuilder.append("DEALER BUSTED, YOU WIN:  ");
            return showResult(stringBuilder);
        }
        else {

            if (hanPlayer.getHardValue() > hanDealer.getHardValue()) {
                stringBuilder.append("YOU WIN: ");

            } else if (hanPlayer.getHardValue() < hanDealer.getHardValue()) {
                stringBuilder.append("YOU LOSE: ");

            } else { stringBuilder.append("PUSH:  "); }
            return showResult(stringBuilder);
        }
    }

    private String showResult(StringBuilder stringBuilder) {

        stringBuilder.append("Dealer's cards:" + hanDealer.showEndState());
        stringBuilder.append("#HAND OVER#");
        return stringBuilder.toString();
    }

    public void setMoney(Double bet) {
        ply.setMoney(bet);
    }

    public Player getPly() {
        return ply;
    }

    public Hand getHanPlayer() {
        return hanPlayer;
    }
}

