package blackjack;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class ImageJPanel extends JPanel {
    private BufferedImage image;

    public static void main(String[] args) {
        JFrame frame = new JFrame("BlackJackGame");
        ImageJPanel imgPanel = new ImageJPanel();
        frame.add(imgPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setSize(400, 300);
    }
    public ImageJPanel() {
        try {
            String strRelativeFilePath = "/src/img/img_mTable.jpg";
            String strPathImg = System.getProperty("user.dir") + strRelativeFilePath;
            File file = new File(strPathImg);
            image = ImageIO.read(file);
        } catch (IOException ex) {
            // handle exception...
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, 400, 300, null); // see javadoc for more info on the parameters
    }

}