package blackjack;

public class Dealer  {

    private Shoe sho;
    private Hand hanDealer;
    private Hand hanPlayer;

    public Dealer(Shoe sho, Hand hanDealer, Hand hanPlayer) {

        this.sho = sho;
        this.hanDealer = hanDealer;
        this.hanPlayer = hanPlayer;
    }


    public void autoHit(){
        while(hanDealer.getHardValue()<=21){
            hanDealer.hit(sho.deal());
        }
    }


    public void hitPlayer(){
        hanPlayer.hit(sho.deal());
    }

    public void hitDealer(){
        hanDealer.hit(sho.deal());
    }

}
